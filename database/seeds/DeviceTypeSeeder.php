<?php

use Illuminate\Database\Seeder;
use SoftAWARE\Models\DeviceType as DeviceType; // to use Eloquent Model
use Carbon\Carbon;

class DeviceTypeSeeder extends Seeder {

	public function run()
	{
		// clear table
		DeviceType::truncate();
		// 1st row
		DeviceType::create( [
			'type' => 'Desktop',
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		// 2nd row
		DeviceType::create( [
			'type' => 'Laptop',
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		// 3rd row
		DeviceType::create( [
			'type' => 'Server (Dedicated/VPS)',
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		// 4th row
		DeviceType::create( [
			'type' => 'Mobile',
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		// 5th row
		DeviceType::create( [
			'type' => 'Tablet',
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		// 6th row
		DeviceType::create( [
			'type' => 'Other',
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		// 7th row
		DeviceType::create( [
			'type' => 'Printer',
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		// 8th row
		DeviceType::create( [
			'type' => 'Scanner',
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		// 9th row
		DeviceType::create( [
			'type' => 'Photocopier',
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		// 10th row
		DeviceType::create( [
			'type' => 'AIO (Printer/Scanner/Copier)',
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		// 11th row
		DeviceType::create( [
			'type' => 'NAS',
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		// 12th row
		DeviceType::create( [
			'type' => 'Router',
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		// 13th row
		DeviceType::create( [
			'type' => 'Switch',
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		// 14th row
		DeviceType::create( [
			'type' => 'Monitor',
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		// 15th row
		DeviceType::create( [
			'type' => 'UPS',
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
	}
}