<?php

use Illuminate\Database\Seeder;
use SoftAWARE\Models\OperatingSystem as OS; // to use Eloquent Model
use Carbon\Carbon;

class OperatingSystemSeeder extends Seeder {

    public function run()
    {
        // clear table
        OS::truncate();
        // 1st row
        OS::create( [
            'id' => 1,
            'manufacturers_id' => 8,
            'os_name' => 'Windows 7',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ] );
        // 2nd row
        OS::create( [
            'id' => 2,
            'manufacturers_id' => 8,
            'os_name' => 'Windows 8',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ] );
        // 3rd row
        OS::create( [
            'id' => 3,
            'manufacturers_id' => 1,
            'os_name' => 'Windows 8.1',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ] );
        // 4th row
        OS::create( [
            'id' => 4,
            'manufacturers_id' => 2,
            'os_name' => 'OSX Yosemite (10.10)',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ] );
        // 5th row
        OS::create( [
            'id' => 5,
            'manufacturers_id' => 2,
            'os_name' => 'OSX Mavericks (10.9)',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ] );
        // 6th row
        OS::create( [
            'id' => 6,
            'manufacturers_id' => 2,
            'os_name' => 'iOS 7',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ] );
        // 7th row
        OS::create( [
            'id' => 7,
            'manufacturers_id' => 2,
            'os_name' => 'iOS 8',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ] );
        // 8th row
        OS::create( [
            'id' => 8,
            'manufacturers_id' => 4,
            'os_name' => 'Android KitKat (4.4)',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ] );
        // 9th row
        OS::create( [
            'id' => 9,
            'manufacturers_id' => 4,
            'os_name' => 'Android Lollipop',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ] );
        // 10th row
        OS::create( [
            'id' => 10,
            'manufacturers_id' => 8,
            'os_name' => 'Windows Phone 8',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ] );
        // 11th row
        OS::create( [
            'id' => 11,
            'manufacturers_id' => 1,
            'os_name' => 'Ubuntu Linux 12.04',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ] );
        // 12th row
        OS::create( [
            'id' => 12,
            'manufacturers_id' => 1,
            'os_name' => 'Ubuntu Linux 14.04',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ] );
        // 13th row
        OS::create( [
            'id' => 13,
            'manufacturers_id' => 1,
            'os_name' => 'Custom Firmware',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ] );
    }
}