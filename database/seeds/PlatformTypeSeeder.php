<?php

use Illuminate\Database\Seeder;
use SoftAWARE\Models\PlatformType as Platform; // to use Eloquent Model
use Carbon\Carbon;

class SoftwareTypeSeeder extends Seeder {

    public function run()
    {
        // clear table
        Platform::truncate();
        // 1st row
        Platform::create( [
            'type' => 'Desktop/Laptop/Server',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ] );
        // 2nd row
        Platform::create( [
            'type' => 'Mobile/Tablet App',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ] );
        // 3rd row
        Platform::create( [
            'type' => 'Web Based (SaaS)',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ] );
    }
}