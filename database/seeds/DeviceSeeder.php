<?php

use Illuminate\Database\Seeder;
use SoftAWARE\Models\Device as Device; // to use Eloquent Model
use Carbon\Carbon;

class DeviceSeeder extends Seeder {

	public function run()
	{
		// clear table
		Device::truncate();

		// 1st row
		Device::create( [
			'device_types_id' => 1,
			'shortname' => "Optimus Prime",
			'model' => 'iMac i3, 3.2GHz',
			'serial_num' => 'W88401231AX',
			'manufacturers_id' => 2,
			'os_id' => 1,
			'cpu' => '3.2',
			'ram' => '3',
			'ram_unit' => 1,
			'storage' => '500',
			'storage_unit' => 1,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		Device::create( [
			'device_types_id' => 1,
			'shortname' => "bumblebee",
			'model' => 'iMac Quad Core Intel Xeon 2.8GHz, 2GB',
			'serial_num' => 'C02CG123DC79',
			'manufacturers_id' => 2,
			'os_id' => 1,
			'cpu' => '2.8',
			'ram' => '2',
			'ram_unit' => 1,
			'storage' => '500',
			'storage_unit' => 1,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		Device::create( [
			'device_types_id' => 1,
			'shortname' => "steeljaw",
			'model' => 'Mac Book Pro',
			'serial_num' => 'V731557CMVZ',
			'manufacturers_id' => 2,
			'os_id' => 1,
			'cpu' => '2.6',
			'ram' => '8',
			'ram_unit' => 1,
			'storage' => '1',
			'storage_unit' => 2,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		Device::create( [
			'device_types_id' => 1,
			'shortname' => "underbite",
			'model' => 'iMac Intel Core 2 Duo 2ghz, 1GB',
			'serial_num' => 'U21516EALG6',
			'manufacturers_id' => 2,
			'os_id' => 1,
			'cpu' => '2',
			'ram' => '1',
			'ram_unit' => 1,
			'storage' => '500',
			'storage_unit' => 1,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		Device::create( [
			'device_types_id' => 1,
			'shortname' => "strongarm",
			'model' => 'Dual Core Intel Xeon, 2x2.6GHz, 1GB',
			'serial_num' => '5E7098YJVTE',
			'manufacturers_id' => 2,
			'os_id' => 1,
			'cpu' => '2.26',
			'ram' => '1',
			'ram_unit' => 1,
			'storage' => '500',
			'storage_unit' => 1,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
	}
}