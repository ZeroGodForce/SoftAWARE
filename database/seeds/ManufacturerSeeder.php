<?php

use Illuminate\Database\Seeder;
use SoftAWARE\Models\Manufacturer as Manufacturer; // to use Eloquent Model
use Carbon\Carbon;

class ManufacturerSeeder extends Seeder {

	public function run()
	{
		// clear table
		Manufacturer::truncate();
		
		//  row
		Manufacturer::create( [
			'id' => 1,
			'manufacturer' => 'Amazon',
			'makes_hardware' => 1,
			'develops_software' => 0,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		//  row
		Manufacturer::create( [
			'id' => 2,
			'manufacturer' => 'Apple',
			'makes_hardware' => 1,
			'develops_software' => 1,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		//  row
		Manufacturer::create( [
			'id' => 3,
			'manufacturer' => 'Dell',
			'makes_hardware' => 1,
			'develops_software' => 0,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );		
		//  row
		Manufacturer::create( [
			'id' => 4,
			'manufacturer' => 'Google',
			'makes_hardware' => 1,
			'develops_software' => 1,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		//  row
		Manufacturer::create( [
			'id' => 5,
			'manufacturer' => 'HP',
			'makes_hardware' => 1,
			'develops_software' => 0,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		//  row
		Manufacturer::create( [
			'id' => 6,
			'manufacturer' => 'IBM',
			'makes_hardware' => 1,
			'develops_software' => 0,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		//  row
		Manufacturer::create( [
			'id' => 7,
			'manufacturer' => 'LG Electronics',
			'makes_hardware' => 1,
			'develops_software' => 0,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );		
		//  row
		Manufacturer::create( [
			'id' => 8,
			'manufacturer' => 'Microsoft',
			'makes_hardware' => 1,
			'develops_software' => 1,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		//  row
		Manufacturer::create( [
			'id' => 9,
			'manufacturer' => 'NEC',
			'makes_hardware' => 1,
			'develops_software' => 0,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		Manufacturer::create( [
			'id' => 10,
			'manufacturer' => 'Netgear',
			'makes_hardware' => 1,
			'develops_software' => 0,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		Manufacturer::create( [
			'id' => 11,
			'manufacturer' => 'Cisco',
			'makes_hardware' => 1,
			'develops_software' => 0,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		//  row
		Manufacturer::create( [
			'id' => 12,
			'manufacturer' => 'Panasonic',
			'makes_hardware' => 1,
			'develops_software' => 0,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		//  row
		Manufacturer::create( [
			'id' => 13,
			'manufacturer' => 'Sony',
			'makes_hardware' => 1,
			'develops_software' => 0,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		//  row
		Manufacturer::create( [
			'id' => 14,
			'manufacturer' => 'Toshiba',
			'makes_hardware' => 1,
			'develops_software' => 0,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
		Manufacturer::create( [
			'id' => 15,
			'manufacturer' => 'Canonical',
			'makes_hardware' => 1,
			'develops_software' => 0,
			'created_at' => Carbon::now()->toDateTimeString(),
			'updated_at' => Carbon::now()->toDateTimeString()
		] );
	}
}









