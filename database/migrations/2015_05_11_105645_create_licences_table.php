<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicencesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('licences', function(Blueprint $table)
		{
			$table->increments('id');
            $table->tinyInteger('software_id');
            $table->tinyInteger('max_assignments')->nullable(); // For multi-user licence keys, nullable if unknown
            $table->string('licence_key')->nullable(); // Nullable in case unknown or using alternative
            $table->string('licence_file')->nullable(); // Nullable in case unknown or using alternative
            $table->date('exp_date')->nullable(); // Nullable in case it's perpetual
            $table->date('purchase_date')->nullable(); // Nullable in case unknown or using alternative
            $table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('licences');
	}

}
