<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('devices', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('shortname')->nullable();
            $table->smallInteger('device_types_id');
            $table->string('asset_id')->nullable();
            $table->string('model')->nullable();
            $table->string('serial_num')->nullable();
            $table->smallInteger('manufacturers_id')->nullable();
            $table->smallInteger('os_id')->nullable();
            $table->float('cpu')->nullable();
	        $table->float('ram')->nullable();
            $table->tinyInteger('ram_unit')->nullable();
            $table->smallInteger('storage')->nullable();
            $table->tinyInteger('storage_unit')->nullable();
            $table->date('purchase_date')->nullable();
            $table->text('notes')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('devices');
	}

}
