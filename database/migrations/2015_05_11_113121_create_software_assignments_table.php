<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoftwareAssignmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('software_assignments', function(Blueprint $table)
		{
			$table->increments('id');
            $table->tinyInteger('licence_id');
            $table->tinyInteger('device_id');
            $table->date('assignment_from')->nullable(); // Nullable in case unknown
            $table->text('notes')->nullable();
            $table->softDeletes();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('software_assignments');
	}

}
