<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoftwareTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('software', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('version')->nullable(); // Nullable in case of SaaS
			$table->smallInteger('manufacturers_id');
			$table->smallInteger('os_id');
			$table->smallInteger('type_id'); // Desktop/Laptop/Server/Mobile/Tablet/Web Based (SaaS)
			$table->smallInteger('num_licences')->nullable(); // Nullable in case it's unknown
			$table->text('notes')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('software');
	}

}
