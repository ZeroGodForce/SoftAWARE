<?php namespace SoftAWARE\Http\Controllers;

use SoftAWARE\Http\Requests;
use SoftAWARE\Http\Controllers\Controller;
use SoftAWARE\Models\PlatformType;
use SoftAWARE\Models\Software;
use SoftAWARE\Models\OperatingSystem;
use SoftAWARE\Models\Manufacturer;

use Illuminate\Http\Request;
use SoftAWARE\Models\SoftwareType;

class SoftwareController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$software = Software::with('manufacturer', 'operatingSystem')->get();

		return view('software.list', compact('software'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$software = New Software();
		$software->developer = Manufacturer::lists('manufacturer', 'id');
		$software->os_list = OperatingSystem::lists('os_name', 'id');
		$software->platform_type_list = PlatformType::lists('type', 'id');

		return view('devices.create', compact('device'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
