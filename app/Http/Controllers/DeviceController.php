<?php namespace SoftAWARE\Http\Controllers;

use SoftAWARE\Http\Requests;
use SoftAWARE\Http\Controllers\Controller;
use SoftAWARE\Models\Device;
use SoftAWARE\Models\DeviceType;
use SoftAWARE\Models\OperatingSystem;
use SoftAWARE\Models\Manufacturer;
use Illuminate\Http\Request;
use Carbon\Carbon;


class DeviceController extends Controller {

    public function __construct()
    {
//        $this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$devices = Device::with('deviceType', 'manufacturer', 'operatingSystem')->get();

        return view('devices.list', compact('devices'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$device = New Device;
		$device->manufacturer_list = Manufacturer::lists('manufacturer', 'id');
		$device->os_list = OperatingSystem::lists('os_name', 'id');
		$device->device_type_list = DeviceType::lists('type', 'id');

        return view('devices.create', compact('device'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $this->validate($request, ['shortname' => 'required|unique:devices', 'type_id' => 'required', 'model' => 'required', 'manufacturer_id' => 'required' ]);

        $device = New Device;
        $device->shortname = strtolower(str_slug($request->shortname, "-"));
        $device->device_types_id = $request->type_id;
        $device->asset_id = $request->asset_id;
        $device->model = $request->model;
        $device->serial_num = $request->serial_num;
        $device->manufacturers_id = $request->manufacturer_id;
        $device->os_id = $request->os_id;
        $device->cpu = $request->cpu;
        $device->ram = $request->ram;
        $device->storage = $request->storage;
        $device->purchase_date = $request->purchase_date;
        $device->notes = $request->notes;

        $device->save();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $device = Device::findOrFail($id);
		$device->manuf = Manufacturer::lists('manufacturer', 'id');
		$device->osname = OperatingSystem::lists('os_name', 'id');

//		return view('devices.create', compact('data'));
        return view('devices.edit', compact('device'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function update($id, Request $request)
    {
        $this->validate($request, ['shortname' => 'required|unique:devices', 'type_id' => 'required', 'model' => 'required', 'manufacturer_id' => 'required' ]);

        $device = Device::findOrFail($id);
        $device->shortname = strtolower(str_slug($request->shortname, "-"));
        $device->device_types_id = $request->type_id;
        $device->asset_id = $request->asset_id;
        $device->model = $request->model;
        $device->serial_num = $request->serial_num;
        $device->manufacturers_id = $request->manufacturer_id;
        $device->os_id = $request->os_id;
        $device->cpu = $request->cpu;
        $device->ram = $request->ram;
        $device->storage = $request->storage;
        $device->purchase_date = $request->purchase_date;
        $device->notes = $request->notes;

        $device->save();

        return redirect('/devices');
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
