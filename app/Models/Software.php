<?php namespace SoftAWARE\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Software extends Model {

	use SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name','version','vendor','platform','num_licences'];

	protected $table = 'software';

	/**
	 * Enable soft deletes.
	 *
	 * @var array
	 */
	protected $dates = ['deleted_at'];

}
