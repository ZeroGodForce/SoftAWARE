<?php namespace SoftAWARE\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlatformType extends Model {

	use SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['type'];

	/**
	 * Enable soft deletes.
	 *
	 * @var array
	 */
	protected $dates = ['deleted_at'];

}
