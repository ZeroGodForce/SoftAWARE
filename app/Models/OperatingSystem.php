<?php namespace SoftAWARE\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OperatingSystem extends Model {

	use SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['os_name'];

	/**
	 * Enable soft deletes.
	 *
	 * @var array
	 */
	protected $dates = ['deleted_at'];

	public function device()
	{
		return $this->belongsTo('Device');
	}

}
