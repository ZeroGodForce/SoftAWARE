<?php namespace SoftAWARE\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeviceType extends Model {

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type'];

    /**
     * Enable soft deletes.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	public function device()
	{
		return $this->belongsTo('Device');
	}

}