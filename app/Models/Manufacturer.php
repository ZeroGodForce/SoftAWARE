<?php namespace SoftAWARE\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Manufacturer extends Model {

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['manufacturer', 'makes_hardware', 'writes_software'];

    /**
     * Enable soft deletes.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	public function device()
	{
		return $this->belongsTo('Device');
	}

}