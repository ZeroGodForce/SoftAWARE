<?php namespace SoftAWARE\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SoftwareAssignment extends Model {

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['licence_id','device_id','assignment_from','notes'];

    /**
     * Enable soft deletes.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

}
