<?php namespace SoftAWARE\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserGroups extends Model {

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['group'];

    /**
     * Enable soft deletes.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

}
