<?php namespace SoftAWARE\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use SoftAWARE\Models\DeviceType;

class Device extends Model {

	use SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['shortname','device_types_id','asset_id','model','serial_num','manufacturer_id','os_id','cpu','ram','storage','purchase_date','notes'];

    /**
     * Enable soft deletes.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
//	protected $casts = [
//		'manufacturers_id' => 'int',
//	];


	/**
	 * @return mixed
	 */
	public function deviceType() {

		return $this->hasOne('SoftAWARE\Models\DeviceType', 'id', 'device_types_id');

	}

	/**
	 * @return mixed
	 */
	public function manufacturer() {

//		return $this->hasOne('SoftAWARE\Models\Manufacturer', 'id', 'manufacturers_id');
		return $this->belongsTo('SoftAWARE\Models\Manufacturer');

	}

	/**
	 * @return mixed
	 */
	public function operatingSystem() {

		return $this->hasOne('SoftAWARE\Models\OperatingSystem', 'id', 'os_id');

	}

}
