# SoftAWARE v4.0 alpha alpha alpha 0.1. alpha.
## - A simple inventory tool for the harassed developer

### Version 4?! WTF happened to versions 1,2, and 3?

I know right? Truth is, I've been writing and re-writing this system for about 5 years now, and never quite managed to get it presentable enough for consumption by other humans.  So, why the hell do you need this when there are a million and one inventory trackers - or you can set up a google spreadsheet?

Simple, google spreadsheets suck for historical tracking and running reports unless you're prepared to spend a day configuring stupid rules - when you'd rather be coding instead.

Look, I'm sure we've all been there. You're hired to be a developer, but you seem to be spending an increasing amount of time answering questions such as, "my email's not working" or, "can you google what this product does for me", or "do we have spare licence of [INSERT EXPENSIVE PROGRAM HERE]"

Well, I can't help you with the first two -  you're on your own there son. However, as far as how many licences of microsoft office do we have left, or can you go round and find out the spec of each computer, then put all the results in some crappy google spreadsheet, and then spend more time listing/highlighting the bits your boss is interested - assuming they are at all.  That I can do something about.


Unlike spiceworks, or assetworks, or some other inventory tracking system ending in 'works', SoftAWARE doesn't have a bunch of fancy features you don't need or have time to set up. you simply, login, and find the info you're looking for in 5 clicks or less guarenteed*


### License

A proper licence is pending, but it will be along the lines of this:

This tool is open source and free for personal or internal use (i.e. specifically for use within your company intranet/network). Modify, change and remix it all you like. However, if you want to sell it - as is or with any kind of modification at all; you must buy a commercial licence. You must also provide attribution (in the footer, or a credits page is fine - I don’t care as long as there is some visible way on your page to view it)

It comes “as is”, if it screws up, it’s your problem - not mine, so make sure you backup regularly. By using it, you absolve me of any liability including (but not limited to) loss of business, loss of money, loss of house, or wife or kids or sanity, or your life, and so on.

In summary, just don’t be a douche.  I’m hopefully saving you grief so please don’t rip me off, otherwise I will come round and kick you in the genes.


* [not guarenteed at all, but I'll try my best]