@extends('layouts.hud')

@section('content')
<section id="main-content">
    <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Software</h3>
        <div class="row mt">
            <div class="col-lg-12">
                <div class="content-panel">
                    <h4><i class="fa fa-angle-right"></i> <a href="/software/create">Add New Program</a></h4>
                    <section id="no-more-tables">
                        <table class="table table-bordered table-striped table-condensed cf table-hover">
                            <thead class="cf">
                            <tr>
                                <th><i class="fa fa-bookmark"></i>Name</th>
                                <th><i class="fa fa-bookmark"></i>Version</th>
                                <th><i class="fa fa-bookmark"></i>Developer</th>
                                <th><i class="fa fa-bookmark"></i>OS Platform</th>
                                <th><i class="fa fa-bookmark"></i>Software Type</th>
                                <th><i class="fa fa-bookmark"></i>No. of Licences</th>
                                <th><i class="fa fa-bookmark"></i>Type</th>
                                <th><i class="fa fa-bookmark"></i>Notes</th>
                                <th><i class="fa fa-bookmark"></i>Actions</th>
                            </tr>
                            </thead>
                            <tbody>


                            @foreach($software as $program)
                                <tr>
                                    <td data-title="name">{{ $program['name'] }}</td>
                                    <td data-title="version">{{ $program['version'] or 'N/A' }}</td>
                                    <td data-title="developer">{{ $program['manufacturer']['manufacturer'] }}</td>
                                    <td data-title="os">{{ $program['operatingSystem']['os_name'] }}</td>
                                    <td data-title="type_id">{{ $program['programType']['type'] }}</td>
                                    <td data-title="model">{{ $program['model'] or 'UNKNOWN' }}</td>
                                    <td data-title="num_licences">{{ $program['num_licences'] or 'UNKNOWN'}}</td>
                                    <td data-title="purchase_date">{{ $program['purchase_date'] or 'UNKNOWN' }}</td>
                                    <td data-title="notes">{{ $program['notes'] }}</td>
                                    <td>
                                        <a href="" data-title="success" class="btn btn-success btn-xs"><i class="fa fa-check"></i></a>
                                        <a href="/software/{{ $program['id'] }}/edit" data-title="primary" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                                        <a href="" data-title="danger" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </section>
                </div><!-- /content-panel -->
            </div><!-- /col-lg-12 -->
        </div><!-- /row -->

    </section><!--/wrapper -->
</section><!-- /MAIN CONTENT -->


@stop