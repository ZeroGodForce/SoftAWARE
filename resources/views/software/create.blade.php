@extends('layouts.hud')

@section('content')

    <section id="main-content">
        <section class="wrapper">
            <h3><i class="fa fa-angle-right"></i> Add Device</h3>

            <div class="row mt">
                <div class="col-lg-12">
                    <div class="form-panel">
                        <h4 class="mb"><a href="/devices"><i class="fa fa-angle-double-left"></i> Back to device list</a></h4>

                        @include ('errors.forms')

                        {!! Form::open(['url' => '/devices', 'class' => 'form-horizontal style-form']) !!}

                            @include ('layouts.forms.device', ['submitText' => 'Add New Device'])

                        {!! Form::close() !!}

                    </div>
                </div><!-- col-lg-12-->
            </div><!-- /row -->

        </section><! --/wrapper -->
    </section><!-- /MAIN CONTENT -->

@stop