



<div class="form-group">
	{!! Form::label('manufacturers_id', 'Developer', ['class'=> 'col-sm-2 col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::select('manufacturers_id', $software->developer, null, ['class'=> 'form-control']) !!}
	</div>
</div>



<div class="form-group">
	{!! Form::label('name', 'Name', ['class'=> 'col-sm-2 col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('name', null, ['class'=> 'form-control']) !!}
	</div>
</div>


<div class="form-group">
	{!! Form::label('version', 'Version', ['class'=> 'col-sm-2 col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('version', null, ['class'=> 'form-control']) !!}
	</div>
</div>


<div class="form-group">
	{!! Form::label('platform', 'Platform', ['class'=> 'col-sm-2 col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('platform', null, ['class'=> 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('serial_num', 'Serial Number', ['class'=> 'col-sm-2 col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('serial_num', null, ['class'=> 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('manufacturers_id', 'Manufacturer', ['class'=> 'col-sm-2 col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::select('manufacturers_id', $device->manufacturer_list, null, ['class'=> 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('os_id', 'Operating System', ['class'=> 'col-sm-2 col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::select('os_id', $device->os_list, null, ['class'=> 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('cpu', 'Processor', ['class'=> 'col-sm-2 col-sm-2 control-label']) !!}
	<div class="col-sm-1">
		{!! Form::number('cpu', null, ['step' => '0.1', 'class' => 'form-control'] ) !!}
	</div>
	<div class="col-sm-1">
		<span class="static-unit">GHz</span>
	</div>
</div>

<div class="form-group">
	{!! Form::label('ram', 'RAM', ['class'=> 'col-sm-2 col-sm-2 control-label']) !!}
	<div class="col-sm-1">
		{!! Form::text('ram', null, ['class'=> 'form-control']) !!}
	</div>
	<div class="col-sm-1">
		{!! Form::select('ram_unit', array('1' => 'GB', '2' => 'MB'), 1, ['step' => '0.1', 'class' => 'form-control'] ) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('storage', 'Storage', ['class'=> 'col-sm-2 col-sm-2 control-label']) !!}
	<div class="col-sm-1">
		{!! Form::text('storage', null, ['class'=> 'form-control']) !!}
	</div>
	<div class="col-sm-1">
		{!! Form::select('storage_unit', array('1' => 'GB', '2' => 'TB'), 1, ['class' => 'form-control'] ) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('purchase_date', 'Purchase Date', ['class'=> 'col-sm-2 col-sm-2 control-label']) !!}
	<div class="col-sm-2">
		{!! Form::date('purchase_date', null, ['class'=> 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('notes', 'Notes', ['class'=> 'col-sm-2 col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::textarea('notes', null, ['class'=> 'form-control', 'id' => 'notes']) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-12">
		{!! Form::submit($submitText, ['class'=> 'btn btn-primary form-control']) !!}
	</div>
</div>