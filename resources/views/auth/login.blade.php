<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.head')
</head>

<body>

    <!-- **********************************************************************************************************************************************************
    MAIN CONTENT
    *********************************************************************************************************************************************************** -->

    <div id="login-page">
        <div class="container">

            <form class="form-login" role="form" method="POST" action="{{ url('/auth/login') }}">

                <h2 class="form-login-heading">sign in now</h2>
                <div class="login-wrap">

                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>RED ALERT!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="User ID" autofocus>
                    <br>
                    <input type="password" class="form-control" name="password" placeholder="Password">



                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember"> Remember Me
                                </label>
                            </div>
                        </div>
                    </div>

                    <label class="checkbox">
		                <span class="pull-right">
                            <a href="{{ url('/password/email') }}">Forgot Password?</a>

		                </span>
                    </label>
                    <button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
                    <hr>

                    <div class="registration">
                        Don't have an account yet?<br/>
                        <a href="{{ url('/auth/register') }}">
                            Create an account
                        </a>
                    </div>
            </form>
        </div>
    </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{{ asset('/assets/js/jquery.js') }}"></script>
    <script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="{{ asset('/assets/js/jquery.backstretch.min.js') }}"></script>
    <script>
        $.backstretch("{{ asset('/assets/img/login-bg.jpg') }}", {speed: 500});
    </script>


</body>
</html>



