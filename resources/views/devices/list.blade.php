@extends('layouts.hud')

@section('content')
<section id="main-content">
    <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Devices</h3>
        <div class="row mt">
            <div class="col-lg-12">
                <div class="content-panel">
                    <h4><i class="fa fa-angle-right"></i> <a href="/devices/create">Add New Device</a></h4>
                    <section id="no-more-tables">
                        <table class="table table-bordered table-striped table-condensed cf table-hover">
                            <thead class="cf">
                            <tr>
                                <th><i class="fa fa-bookmark"></i>Shortname</th>
                                <th><i class="fa fa-bookmark"></i>Asset ID</th>
                                <th><i class="fa fa-bookmark"></i>Model</th>
                                <th><i class="fa fa-bookmark"></i>Serial Number</th>
                                <th><i class="fa fa-bookmark"></i>Manufacturer</th>
                                <th><i class="fa fa-bookmark"></i>Operating System</th>
                                <th><i class="fa fa-bookmark"></i>Type</th>
                                <th><i class="fa fa-bookmark"></i>Purchase Date</th>
                                <th><i class="fa fa-bookmark"></i>Notes</th>
                                <th><i class="fa fa-bookmark"></i>Actions</th>
                            </tr>
                            </thead>
                            <tbody>


                            @foreach($devices as $device)
                                <tr>
                                    <td data-title="shortname">{{ $device['shortname'] }}</td>
                                    <td data-title="assetid">{{ $device['assetid'] or 'N/A' }}</td>
                                    <td data-title="model">{{ $device['model'] or 'UNKNOWN' }}</td>
                                    <td data-title="serial_num">{{ $device['serial_num'] or 'UNKNOWN'}}</td>
                                    <td data-title="vendor">{{ $device['manufacturer']['manufacturer'] }}</td>
                                    <td data-title="os">{{ $device['operatingSystem']['os_name'] }}</td>
                                    <td data-title="type_id">{{ $device['deviceType']['type'] }}</td>
                                    <td data-title="purchase_date">{{ $device['purchase_date'] or 'UNKNOWN' }}</td>
                                    <td data-title="notes">{{ $device['notes'] }}</td>
                                    <td>
                                        <a href="" data-title="success" class="btn btn-success btn-xs"><i class="fa fa-check"></i></a>
                                        <a href="/devices/{{ $device['id'] }}/edit" data-title="primary" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                                        <a href="" data-title="danger" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </section>
                </div><!-- /content-panel -->
            </div><!-- /col-lg-12 -->
        </div><!-- /row -->

    </section><!--/wrapper -->
</section><!-- /MAIN CONTENT -->


@stop